package com.ayoosh.profilemanagement;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileRepository extends JpaRepository<EmployeeProfile, Integer> {

}
