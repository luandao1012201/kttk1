package com.ayoosh.profilemanagement;

import java.util.List;

public interface EmployeeProfileService {
    void addEmployeeProfile(EmployeeProfile profile);
    List<EmployeeProfile> getEmployeeProfiles();
}
